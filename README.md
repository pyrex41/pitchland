# Primary Demo -- Screencast
[![Code Demo Video](https://img.youtube.com/vi/TMbCMv2aGXg/0.jpg)](https://youtu.be/TMbCMv2aGXg "Code Demo")

# Simulation Video
[![Simulation Video](https://img.youtube.com/vi/tnwMXo5YM14/0.jpg)](https://youtu.be/tnwMXo5YM14 "Simulation Demo")

## MVP / Alpha Version Repository
### [Vyper Repo](https://gitlab.com/pyrex41/peg_token)

## Demo Implementation in Julia for Simulation and Exploration
### [Julia Repo](https://gitlab.com/pyrex41/pegdemo)
